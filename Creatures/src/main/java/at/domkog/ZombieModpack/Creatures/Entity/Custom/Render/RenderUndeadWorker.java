package at.domkog.ZombieModpack.Creatures.Entity.Custom.Render;

import at.domkog.ZombieModpack.Creatures.Entity.Custom.Render.Models.ModelCreatureZombie;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class RenderUndeadWorker extends RenderLiving {

	private static final ResourceLocation zombieTextures = new ResourceLocation("zombiemodpackcreatures:textures/entities/ZombieWorker.png");
	
	public RenderUndeadWorker() {
		super(new ModelCreatureZombie(), 0.5F);
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity p_110775_1_) {
		return zombieTextures;
	}

}
