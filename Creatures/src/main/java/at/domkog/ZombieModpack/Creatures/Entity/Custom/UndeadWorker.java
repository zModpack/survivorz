package at.domkog.ZombieModpack.Creatures.Entity.Custom;

import net.minecraft.init.Items;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import at.domkog.ZombieModpack.Creatures.Entity.CreatureAttributes;
import at.domkog.ZombieModpack.Creatures.Entity.CreatureZombie;
import at.domkog.ZombieModpack.Creatures.Entity.ZombieType;
import at.domkog.ZombieModpack.Creatures.Entity.AI.Noise.EntityAINoise;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.WorkerCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.ZombieCorpse;

public class UndeadWorker extends CreatureZombie {

	public UndeadWorker(World p_i1745_1_) {
		super(p_i1745_1_);
		this.tasks.addTask(10, new EntityAINoise(this, 40));
		this.applyEntityAttributes(new CreatureAttributes().setAttackDamage(6D).setFollowRange(40D).setKnockbackResistance(0.1D).setMaxHealth(20D).setMovementSpeed(0.23000000417232513D));
	}

	@Override
	protected void dropFewItems(boolean par1, int par2) {
		if(this.rand.nextInt(3) == 0) {
			this.dropItem(Items.bone, 1);
			if(this.rand.nextInt(2) == 0) this.dropItem(Items.stone_shovel, 1);
			if(this.rand.nextInt(6) == 0) this.dropItem(Items.stone_pickaxe, 1);
		}
	}

	@Override
	public void dropRareDrop(int p_70600_1_) {}

	@Override
	public void spawnCorpse() {
		WorkerCorpse corpse = new WorkerCorpse(worldObj);
		corpse.setLocationAndAngles(posX, posY, posZ, this.rand.nextFloat() * 360.0F, 0.0F);
		worldObj.spawnEntityInWorld(corpse);
	}

}
