package at.domkog.ZombieModpack.Creatures.Entity;

import java.util.HashMap;

import net.minecraft.entity.SharedMonsterAttributes;

public class CreatureAttributes {

	private HashMap<String, Double> attributes = new HashMap<String, Double>();
	
	public CreatureAttributes() {
		this.attributes.put("maxHealth", 0D);
		this.attributes.put("movementSpeed", 0D);
		this.attributes.put("knockbackResistance", 0D);
		this.attributes.put("followRange", 0D);
		this.attributes.put("attackDamage", 0D);
	}

	public double getMaxHealth() {
		return this.attributes.get("maxHealth");
	}
	
	public CreatureAttributes setMaxHealth(double value) {
		this.attributes.put("maxHealth", value);
		return this;
	}
	
	public double getMovementSpeed() {
		return this.attributes.get("movementSpeed");
	}
	
	public CreatureAttributes setMovementSpeed(double value) {
		this.attributes.put("movementSpeed", value);
		return this;
	}
	
	public double getKnockbackResistance() {
		return this.attributes.get("knockbackResistance");
	}
	
	public CreatureAttributes setKnockbackResistance(double value) {
		this.attributes.put("knockbackResistance", value);
		return this;
	}
	
	public double getFollowRange() {
		return this.attributes.get("followRange");
	}
	
	public CreatureAttributes setFollowRange(double value) {
		this.attributes.put("followRange", value);
		return this;
	}
	
	public double getAttackDamage() {
		return this.attributes.get("attackDamage");
	}
	
	public CreatureAttributes setAttackDamage(double value) {
		this.attributes.put("attackDamage", value);
		return this;
	}
	
}
