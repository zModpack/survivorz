package at.domkog.ZombieModpack.Creatures.Entity.AI;

import at.domkog.ZombieModpack.Creatures.Entity.CreatureZombie;
import cpw.mods.fml.common.registry.GameData;
import net.minecraft.block.Block;
import net.minecraft.block.BlockDoor;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.EntityAIDoorInteract;
import net.minecraft.init.Blocks;
import net.minecraft.pathfinding.PathEntity;
import net.minecraft.pathfinding.PathNavigate;
import net.minecraft.pathfinding.PathPoint;
import net.minecraft.util.MathHelper;

public class EntityAIBlockInteract extends EntityAIBase {

	public static Block[] breakableBlocks = new Block[]{Blocks.stone, Blocks.grass, Blocks.dirt};
	
	protected EntityLiving theEntity;
	protected Block field_151504_e;
	protected int entityPosX;
    protected int entityPosY;
    protected int entityPosZ;
	private boolean hasStoppedBlockInteraction;
	float entityPositionX;
    float entityPositionZ;
    
    public int range = 6;
	
	public EntityAIBlockInteract(EntityLiving p_i1621_1_) {
		super();
		this.theEntity = p_i1621_1_;
	}
	
    public boolean continueExecuting()
    {
        return !this.hasStoppedBlockInteraction;
    }

    /**
     * Execute a one shot task or start executing a continuous task
     */
    public void startExecuting()
    {
    	System.out.println("Started executing");
        this.hasStoppedBlockInteraction = false;
        this.entityPositionX = (float)((double)((float)this.entityPosX + 0.5F) - this.theEntity.posX);
        this.entityPositionZ = (float)((double)((float)this.entityPosZ + 0.5F) - this.theEntity.posZ);
    }

    /**
     * Updates the task
     */
    public void updateTask()
    {
        float f = (float)((double)((float)this.entityPosX + 0.5F) - this.theEntity.posX);
        float f1 = (float)((double)((float)this.entityPosZ + 0.5F) - this.theEntity.posZ);
        float f2 = this.entityPositionX * f + this.entityPositionZ * f1;

        if (f2 < 0.0F)
        {
            this.hasStoppedBlockInteraction = true;
        }
    }

	@Override
	public boolean shouldExecute() {
		EntityLivingBase e = this.theEntity.getAITarget();
		if(e == null) return false;
		System.out.println();
		if(this.theEntity.getDistance(e.posX, e.posY, e.posZ) <= this.range) {
			double distX = 0;
			double distZ = 0;
			if(e.posX > this.theEntity.posX) distX = e.posX - this.theEntity.posX;
			else distX = this.theEntity.posX - e.posX;
			if(e.posZ > this.theEntity.posZ) distZ = e.posZ - this.theEntity.posZ;
			else distZ = this.theEntity.posZ - e.posZ;
			if(distX > distZ) {
				if(e.posX > this.theEntity.posX) {
					for(double i = this.theEntity.posX; i <= e.posX; i++) {
						Block b = this.func_151503_a((int) i, (int) this.theEntity.posY, (int) this.theEntity.posZ);
						if(b != null) {
							this.func_151503_b(b, i, this.theEntity.posY, this.theEntity.posZ);
							return true;
						}
					}
				} else {
					for(double i = this.theEntity.posX; i >= e.posX; i--) {
						Block b = this.func_151503_a((int) i, (int) this.theEntity.posY, (int) this.theEntity.posZ);
						if(b != null) {
							this.func_151503_b(b, i, this.theEntity.posY, this.theEntity.posZ);
							return true;
						}
					}
				}
			} else {
				if(e.posZ > this.theEntity.posY) {
					for(double i = this.theEntity.posZ; i <= e.posZ; i++) {
						Block b = this.func_151503_a((int) this.theEntity.posX, (int) this.theEntity.posY, (int) i);
						if(b != null) {
							this.func_151503_b(b, this.theEntity.posX, this.theEntity.posY, i);
							return true;
						}
					}
				} else {
					for(double i = this.theEntity.posZ; i >= e.posZ; i--) {
						Block b = this.func_151503_a((int) this.theEntity.posX, (int) this.theEntity.posY, (int) i);
						if(b != null) {
							this.func_151503_b(b, this.theEntity.posX, this.theEntity.posY, i);
							return true;
						}
					}
				}
			}
		}
		return false;
		/*if(this.theEntity.getAITarget() == null) return false;
            PathNavigate pathnavigate = this.theEntity.getNavigator();
            PathEntity pathentity =  pathnavigate.getPathToEntityLiving(this.theEntity.getAITarget());;

            if (pathentity != null && !pathentity.isFinished())
            {
            	System.out.println("Iterating path");
                for (int i = 0; i < Math.min(pathentity.getCurrentPathIndex() + 2, pathentity.getCurrentPathLength()); ++i)
                {
                    PathPoint pathpoint = pathentity.getPathPointFromIndex(i);
                    this.entityPosX = pathpoint.xCoord;
                    this.entityPosY = pathpoint.yCoord + 1;
                    this.entityPosZ = pathpoint.zCoord;

                    if (this.theEntity.getDistanceSq((double)this.entityPosX, this.theEntity.posY, (double)this.entityPosZ) <= 2.25D)
                    {
                        this.field_151504_e = this.func_151503_a(this.entityPosX, this.entityPosY, this.entityPosZ);

                        if (this.field_151504_e != null) {
                            return true;
                        } else {
                        	System.out.println("No Block found!");
                        }
                    }
                }

                this.entityPosX = MathHelper.floor_double(this.theEntity.posX);
                this.entityPosY = MathHelper.floor_double(this.theEntity.posY + 1.0D);
                this.entityPosZ = MathHelper.floor_double(this.theEntity.posZ);
                this.field_151504_e = this.func_151503_a(this.entityPosX, this.entityPosY, this.entityPosZ);
                return this.field_151504_e != null;
            }
            else
            {
            	if(pathentity == null) System.out.println("PathEntity == null");
            	else if(pathentity.isFinished()) System.out.println("Finished");
                return false;
            }*/
    }
	
	private Block func_151503_a(int p_151503_1_, int p_151503_2_, int p_151503_3_) {
        Block block = this.theEntity.worldObj.getBlock(p_151503_1_, p_151503_2_, p_151503_3_);
        for(Block b: this.breakableBlocks) {
        	if(Block.getIdFromBlock(block) == Block.getIdFromBlock(b)) return b;
        }
        return null;
    }
	
	private void func_151503_b(Block b, double x, double y, double z) {
		this.entityPosX = (int) x;
        this.entityPosY = (int) y;
        this.entityPosZ = (int) z;
		field_151504_e = b;
	}

}
