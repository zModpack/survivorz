package at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse;

import at.domkog.ZombieModpack.Creatures.Entity.ZombieType;
import net.minecraft.world.World;

public class PolicemanCorpse extends ZombieCorpse {

	public PolicemanCorpse(World p_i1738_1_) {
		super(p_i1738_1_);
	}

	@Override
	public ZombieType getType() {
		return ZombieType.UNDEADPOLICEMAN;
	}

}
