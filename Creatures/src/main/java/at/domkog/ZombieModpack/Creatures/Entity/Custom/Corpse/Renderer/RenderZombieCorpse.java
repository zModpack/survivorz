package at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.Renderer;

import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import at.domkog.ZombieModpack.Creatures.Entity.CreatureZombie;
import at.domkog.ZombieModpack.Creatures.Entity.ZombieType;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.ZombieCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.Models.ModelZombieCorpse;

public class RenderZombieCorpse extends RenderLiving {

	private ResourceLocation texture;
	
	public RenderZombieCorpse() {
		super(new ModelZombieCorpse(), 0.5F);
	}

	@Override
    protected ResourceLocation getEntityTexture(Entity par1Entity) {
        return ((ZombieCorpse) par1Entity).getTexture();
    }
	
}
