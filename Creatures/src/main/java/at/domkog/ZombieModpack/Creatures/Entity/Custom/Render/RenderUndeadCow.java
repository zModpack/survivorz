package at.domkog.ZombieModpack.Creatures.Entity.Custom.Render;

import net.minecraft.client.model.ModelCow;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import at.domkog.ZombieModpack.Creatures.Entity.ZombieType;

public class RenderUndeadCow extends RenderLiving {

	private static final ResourceLocation zombieTextures = new ResourceLocation(ZombieType.UNDEADCOW.getTextureLocation());
	
	public RenderUndeadCow() {
		super(new ModelCow(), 0.5F);
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity p_110775_1_) {
		return zombieTextures;
	}

}

