package at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.Renderer;

import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.ZombieCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.Models.ModelPigCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.Models.ModelZombieCorpse;

public class RenderPigCorpse extends RenderLiving {

	private ResourceLocation texture;
	
	public RenderPigCorpse() {
		super(new ModelPigCorpse(), 0.5F);
	}

	@Override
    protected ResourceLocation getEntityTexture(Entity par1Entity) {
        return ((ZombieCorpse) par1Entity).getTexture();
    }
	
}
