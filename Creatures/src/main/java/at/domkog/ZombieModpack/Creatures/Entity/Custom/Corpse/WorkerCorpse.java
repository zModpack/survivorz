package at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse;

import at.domkog.ZombieModpack.Creatures.Entity.ZombieType;
import net.minecraft.world.World;

public class WorkerCorpse extends ZombieCorpse {

	public WorkerCorpse(World p_i1738_1_) {
		super(p_i1738_1_);
	}

	@Override
	public ZombieType getType() {
		return ZombieType.UNDEADWORKER;
	}

}
