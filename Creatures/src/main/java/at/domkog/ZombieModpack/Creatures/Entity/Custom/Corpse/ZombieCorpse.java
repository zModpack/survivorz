package at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.client.particle.EntitySpellParticleFX;
import net.minecraft.entity.DataWatcher;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import at.domkog.ZombieModpack.Creatures.Entity.ZombieType;

public abstract class ZombieCorpse extends EntityMob {

	private DataWatcher dataWatcher;
	
	public ZombieCorpse(World p_i1738_1_) {
		super(p_i1738_1_);
		this.dataWatcher = this.getDataWatcher();
		this.setSize(1f, 0.45f);
	}
	
	public abstract ZombieType getType();
	
	private void applyAttributes() {
		this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(1000D);
		this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.23000000417232513D);
		this.getEntityAttribute(SharedMonsterAttributes.knockbackResistance).setBaseValue(100D);
		this.getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(0D);
		this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(0D);
	}
	
	@Override
	public void onLivingUpdate() {
		if(this.dataWatcher.getWatchableObjectInt(12) != this.getType().getDecayStages()) {
			if(this.dataWatcher.getWatchableObjectInt(13) >= this.getType().getDecayTicks(this.dataWatcher.getWatchableObjectInt(12))) {
				this.dataWatcher.updateObject(12, (this.dataWatcher.getWatchableObjectInt(12) + 1));
				this.dataWatcher.updateObject(13, 0);
			} else this.dataWatcher.updateObject(13, (this.dataWatcher.getWatchableObjectInt(13) + 1));
		} else {
			if(this.dataWatcher.getWatchableObjectInt(13) >= this.getType().getDestructionTicks()) {
				this.worldObj.removeEntity(this);
			} else this.dataWatcher.updateObject(13, (this.dataWatcher.getWatchableObjectInt(13) + 1));
		}
		if(this.getType().getPotionEffectLength(this.dataWatcher.getWatchableObjectInt(12)) != 0) {
			for(EntityPlayer player: this.getPlayersInRange()) {
				for(PotionEffect effect: this.getType().getPotionEffects(this.dataWatcher.getWatchableObjectInt(12))) player.addPotionEffect(effect);
			}
		}

		/*if (!worldObj.isRemote && ticksExisted%10==0)
		{
			    double motionY = rand.nextGaussian() * 0.0025D;
			    EntityFX particle = new EntitySpellParticleFX(

			          worldObj, 

			          posX + rand.nextFloat() * width * 2.0F - width, 

			          posY + 0.5D + rand.nextFloat() * height, 

			          posZ + rand.nextFloat() * width * 2.0F - width, 

			          0, 

			          motionY, 

			          0);
			    particle.setRBGColorF(0.5f, 0.25f, 0.05f);
			    Minecraft.getMinecraft().effectRenderer.addEffect(particle);
		}*/

		this.moveEntityWithHeading(0F, 0F);
	}
	
	public void entityInit() {
		super.entityInit();
		DataWatcher data = this.getDataWatcher();
		data.addObject(12, (int) 0);
		data.addObject(13, (int) 0);
		data.addObject(14, (int) 0);
	}
	
	public void writeEntityToNBT(NBTTagCompound compound) {
        compound.setInteger("decayStage", this.dataWatcher.getWatchableObjectInt(12));
        compound.setInteger("decayTick", this.dataWatcher.getWatchableObjectInt(13));
        compound.setInteger("burningTime", this.dataWatcher.getWatchableObjectInt(14));
	}
	
	public void readEntityFromNBT(NBTTagCompound compound) {
		int decayStage = compound.getInteger("decayStage");
		int decayTick = compound.getInteger("decayTick");
		int burningTime = compound.getInteger("burningTime");
		DataWatcher data = this.getDataWatcher();
		data.updateObject(12, decayStage);
		data.updateObject(13, decayTick);
		data.updateObject(14, burningTime);
	}
	
	public ResourceLocation getTexture() {
		if(this.dataWatcher.getWatchableObjectInt(12) == 0) return new ResourceLocation(this.getType().getTextureLocation());
		else return new ResourceLocation(this.getType().getDecayTexture(this.dataWatcher.getWatchableObjectInt(12) - 1));
	}
	
	public int getBurningTime() {
		return this.dataWatcher.getWatchableObjectInt(14);
	}
	
	public void setBurningTime(int value) {
		this.dataWatcher.updateObject(14, value);
	}
	
	public List<EntityPlayer> getPlayersInRange() {
		ArrayList<EntityPlayer> result = new ArrayList<EntityPlayer>();
		for(Object e: this.worldObj.getEntitiesWithinAABBExcludingEntity(this, AxisAlignedBB.getBoundingBox(this.posX, this.posY, this.posZ, this.posX + 1, this.posY + 0.45, this.posZ + 1).expand(this.dataWatcher.getWatchableObjectInt(12) / 1.5, this.dataWatcher.getWatchableObjectInt(12) / 1.5, this.dataWatcher.getWatchableObjectInt(12) / 1.5))) {
			if(e instanceof EntityPlayer) result.add((EntityPlayer) e);
		}
		return result;
	}
	
}
