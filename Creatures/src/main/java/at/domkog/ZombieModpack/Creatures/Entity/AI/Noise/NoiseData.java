package at.domkog.ZombieModpack.Creatures.Entity.AI.Noise;

import java.util.HashMap;
import java.util.ListIterator;

import net.minecraft.entity.DataWatcher;
import net.minecraft.entity.DataWatcher.WatchableObject;
import net.minecraft.entity.player.EntityPlayerMP;

public class NoiseData {
	
	private static HashMap<String, NoiseData> playerNoise = new HashMap<String, NoiseData>();
	
	public static void createData(EntityPlayerMP player) {
		playerNoise.put(player.getUniqueID().toString(), new NoiseData(player.getDataWatcher()));
	}
	
	public static boolean hasData(EntityPlayerMP player) {
		return playerNoise.containsKey(player.getUniqueID().toString());
	}
	
	public static NoiseData getData(EntityPlayerMP player) {
		return playerNoise.get(player.getUniqueID().toString());
	}
	
	public static boolean hasData(String UUID) {
		return playerNoise.containsKey(UUID);
	}
	
	public static NoiseData getData(String UUID) {
		return playerNoise.get(UUID);
	}
	
	private DataWatcher dataWatcher;
	private float[] noises;
	
	public NoiseData(DataWatcher dataWatcher) {
		this.noises = new float[5];
		//Register Noise types;
		this.noises[0] = 0.0f; //Step
		this.noises[1] = 0.0f; //Jump
		this.noises[2] = 0.0f; //Break
		this.noises[3] = 0.0f; //Place
		this.noises[4] = 0.0f; //Attack
	}
	
	public float[] getNoises() {
		return this.noises;
	}
	
	public float getNoise(int index) {
		return this.noises[index];
	}
	
	public void setNoise(int index, float value) {
		this.noises[index] = value;
	}
	
	public void addNoise(int index, float value) {
		this.noises[index] += value;
	}
	
	public void subNoise(int index, float value) {
		this.noises[index] -= value;
	}
	
	public float sumNoise() {
		float sum = 0.0f;
		for(float f: this.noises) {
			sum += f;
		}
		return sum;
	}
	
	public DataWatcher getDataWatcher() {
		return this.dataWatcher;
	}
	
	public float getDataWatcherValue() {
		return dataWatcher.getWatchableObjectFloat(20);
	}
	
	public boolean hasWatchableObject() {
		try {
			ListIterator listIterator = dataWatcher.getAllWatched().listIterator();
			while(listIterator.hasNext()) {
				WatchableObject object = (WatchableObject) listIterator.next();
				if(object.getDataValueId() == 20) return true;
			}
		} catch(NullPointerException e) { return false; }
		return false;
	}
	
}
