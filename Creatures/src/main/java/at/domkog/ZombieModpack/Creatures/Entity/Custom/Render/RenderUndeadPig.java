package at.domkog.ZombieModpack.Creatures.Entity.Custom.Render;

import at.domkog.ZombieModpack.Creatures.Entity.ZombieType;
import net.minecraft.client.model.ModelPig;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class RenderUndeadPig extends RenderLiving {

	private static final ResourceLocation zombieTextures = new ResourceLocation(ZombieType.UNDEADPIG.getTextureLocation());
	
	public RenderUndeadPig() {
		super(new ModelPig(), 0.5F);
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity p_110775_1_) {
		return zombieTextures;
	}

}

