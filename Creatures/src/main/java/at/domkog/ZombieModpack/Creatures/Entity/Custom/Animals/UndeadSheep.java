package at.domkog.ZombieModpack.Creatures.Entity.Custom.Animals;

import net.minecraft.world.World;
import at.domkog.ZombieModpack.Creatures.Entity.CreatureAttributes;
import at.domkog.ZombieModpack.Creatures.Entity.CreatureZombie;
import at.domkog.ZombieModpack.Creatures.Entity.AI.Noise.EntityAINoise;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.PigCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.SheepCorpse;

public class UndeadSheep extends CreatureZombie {

	public UndeadSheep(World p_i1745_1_) {
		super(p_i1745_1_);
		this.tasks.addTask(10, new EntityAINoise(this, 40));
		this.applyEntityAttributes(new CreatureAttributes().setAttackDamage(1.5D).setFollowRange(40D).setKnockbackResistance(0.1D).setMaxHealth(20D).setMovementSpeed(0.23000000417232513D));
		this.setSize(1f, 1f);
	}

	@Override
	public void spawnCorpse() {
		SheepCorpse corpse = new SheepCorpse(worldObj);
		corpse.setLocationAndAngles(posX, posY, posZ, this.rand.nextFloat() * 360.0F, 0.0F);
		worldObj.spawnEntityInWorld(corpse);
	}

}
