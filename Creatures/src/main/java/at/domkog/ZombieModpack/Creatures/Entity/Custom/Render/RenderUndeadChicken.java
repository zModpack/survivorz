package at.domkog.ZombieModpack.Creatures.Entity.Custom.Render;

import net.minecraft.client.model.ModelChicken;
import net.minecraft.client.model.ModelCow;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.util.ResourceLocation;
import at.domkog.ZombieModpack.Creatures.Entity.ZombieType;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Animals.UndeadChicken;

public class RenderUndeadChicken extends RenderLiving {

	private static final ResourceLocation zombieTextures = new ResourceLocation(ZombieType.UNDEADCHICKEN.getTextureLocation());
	
	public RenderUndeadChicken() {
		super(new ModelChicken(), 0.5F);
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity p_110775_1_) {
		return zombieTextures;
	}

	@Override
	public void doRender(Entity p_76986_1_, double p_76986_2_, double p_76986_4_, double p_76986_6_, float p_76986_8_, float p_76986_9_) {
        this.doRender((UndeadChicken)p_76986_1_, p_76986_2_, p_76986_4_, p_76986_6_, p_76986_8_, p_76986_9_);
    }
	
}

