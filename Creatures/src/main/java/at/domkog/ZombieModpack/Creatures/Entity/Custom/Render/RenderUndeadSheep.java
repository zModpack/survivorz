package at.domkog.ZombieModpack.Creatures.Entity.Custom.Render;

import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ResourceLocation;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Animals.UndeadSheep;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Render.Models.ModelUndeadSheep1;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Render.Models.ModelUndeadSheep2;

public class RenderUndeadSheep extends RenderLiving {

	private static final ResourceLocation shearedSheepTextures = new ResourceLocation("zombiemodpackcreatures:textures/entities/UndeadSheep_fur.png");
	private static final ResourceLocation sheepTextures = new ResourceLocation("zombiemodpackcreatures:textures/entities/UndeadSheep.png");
	
	public RenderUndeadSheep() {
		super(new ModelUndeadSheep1(), 0.5f);
		this.setRenderPassModel(new ModelUndeadSheep2());
	}

	protected int shouldRenderPass(UndeadSheep p_77032_1_, int p_77032_2_, float p_77032_3_) {
		this.bindTexture(sheepTextures);
		return 1;
    }

	/**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    protected ResourceLocation getEntityTexture(UndeadSheep p_110775_1_)
    {
        return shearedSheepTextures;
    }

    /**
     * Queries whether should render the specified pass or not.
     */
    protected int shouldRenderPass(EntityLivingBase p_77032_1_, int p_77032_2_, float p_77032_3_)
    {
        return this.shouldRenderPass((UndeadSheep)p_77032_1_, p_77032_2_, p_77032_3_);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    protected ResourceLocation getEntityTexture(Entity p_110775_1_)
    {
        return this.getEntityTexture((UndeadSheep)p_110775_1_);
    }
	
}
