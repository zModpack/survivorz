package at.domkog.ZombieModpack.Creatures;

import net.minecraft.entity.EnumCreatureType;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.MinecraftForge;
import at.domkog.ZombieModpack.Creatures.Entity.LivingEntityHandler;
import at.domkog.ZombieModpack.Creatures.Entity.AI.Noise.NoiseEventHook;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.UndeadCitizen;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.UndeadPoliceman;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.UndeadWorker;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Animals.UndeadChicken;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Animals.UndeadCow;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Animals.UndeadPig;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Animals.UndeadSheep;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.ChickenCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.CitizenCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.CowCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.PigCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.PolicemanCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.SheepCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.WorkerCorpse;
import at.domkog.ZombieModpack.Creatures.Proxy.CommonProxy;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.EntityRegistry;
import de.MolaynoxX.ZombieModpack.World.ZombieModpackWorld;

@Mod(modid = Creatures.MODID,version = Creatures.VERSION)
public class Creatures {

	/*
	 * Git:
	 *  1: git add src/
	 *  2: git commit -m "<Nachricht>"
	 *  3: git push origin --all
	 */
	
	public static final String MODID = "ZombieModpackCreatures";
    public static final String VERSION = "InDev-0.2.2";
	
    @SidedProxy(clientSide = "at.domkog.ZombieModpack.Creatures.Proxy.ClientProxy", serverSide = "at.domkog.ZombieModpack.Creatures.Proxy.CommonProxy")
    public static CommonProxy proxy;
    
    //public static Potion potionZombieBite;
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
    	/*Potion[] potionTypes = null;

    	for (Field f : Potion.class.getDeclaredFields()) {
    		f.setAccessible(true);
    		try {
    			if (f.getName().equals("potionTypes") || f.getName().equals("field_76425_a")) {
    				Field modfield = Field.class.getDeclaredField("modifiers");
    				modfield.setAccessible(true);
    				modfield.setInt(f, f.getModifiers() & ~Modifier.FINAL);

    				potionTypes = (Potion[])f.get(null);
    				final Potion[] newPotionTypes = new Potion[256];
    				System.arraycopy(potionTypes, 0, newPotionTypes, 0, potionTypes.length);
    				f.set(null, newPotionTypes);
    			}
    		}
    		catch (Exception e) {
    			System.err.println("Severe error, please report this to the mod author:");
    			System.err.println(e);
    		}
    	}*/
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
    	//Register Zombies
    	EntityRegistry.registerGlobalEntityID(UndeadCitizen.class, "UndeadCitizen", EntityRegistry.findGlobalUniqueEntityId(), (255 << 64), (255 << 64)+ (200 << 32));
    	EntityRegistry.registerGlobalEntityID(UndeadWorker.class, "UndeadWorker", EntityRegistry.findGlobalUniqueEntityId(), (255 << 32), (255 << 32)+ (200 << 16));
    	EntityRegistry.registerGlobalEntityID(UndeadPoliceman.class, "UndeadPoliceman", EntityRegistry.findGlobalUniqueEntityId(), (255 << 16), (255 << 8)+ (200 << 8));
    	EntityRegistry.registerGlobalEntityID(UndeadPig.class, "UndeadPig", EntityRegistry.findGlobalUniqueEntityId(), (255 << 16), (255 << 8)+ (200 << 4));
    	EntityRegistry.registerGlobalEntityID(UndeadCow.class, "UndeadCow", EntityRegistry.findGlobalUniqueEntityId(), (255 << 8), (255 << 4)+ (200 << 2));
    	EntityRegistry.registerGlobalEntityID(UndeadChicken.class, "UndeadChicken", EntityRegistry.findGlobalUniqueEntityId(), (255 << 2), (255 << 2)+ (200 << 1));
    	EntityRegistry.registerGlobalEntityID(UndeadSheep.class, "UndeadSheep", EntityRegistry.findGlobalUniqueEntityId(), (255 << 1), (255 << 1)+ (200 << 1));
    	//Register Corpse
    	EntityRegistry.registerModEntity(CitizenCorpse.class, "CitizenCorpse", 16, this, 80, 1, true);
    	EntityRegistry.registerModEntity(WorkerCorpse.class, "WorkerCorpse", 17, this, 80, 1, true);
    	EntityRegistry.registerModEntity(PolicemanCorpse.class, "PolicemanCorpse", 18, this, 80, 1, true);
    	EntityRegistry.registerModEntity(PigCorpse.class, "PigCorpse", 19, this, 80, 1, true);
    	EntityRegistry.registerModEntity(CowCorpse.class, "CowCorpse", 20, this, 80, 1, true);
    	EntityRegistry.registerModEntity(ChickenCorpse.class, "ChickenCorpse", 21, this, 80, 1, true);
    	EntityRegistry.registerModEntity(SheepCorpse.class, "SheepCorpse", 22, this, 80, 1, true);
    	//Add Zombie spawns
    	
    	EntityRegistry.addSpawn(UndeadCitizen.class, 20, 5, 10, EnumCreatureType.monster, BiomeGenBase.deepOcean, BiomeGenBase.ocean, ZombieModpackWorld.biomeDesertedFlat, ZombieModpackWorld.biomeDesertedHills, ZombieModpackWorld.biomeDesertedLands);
    	EntityRegistry.addSpawn(UndeadWorker.class, 15, 1, 1, EnumCreatureType.monster, BiomeGenBase.deepOcean, BiomeGenBase.ocean, ZombieModpackWorld.biomeDesertedFlat, ZombieModpackWorld.biomeDesertedHills, ZombieModpackWorld.biomeDesertedLands);
    	EntityRegistry.addSpawn(UndeadPoliceman.class, 5, 1, 1, EnumCreatureType.monster, BiomeGenBase.deepOcean, BiomeGenBase.ocean, ZombieModpackWorld.biomeDesertedFlat, ZombieModpackWorld.biomeDesertedHills, ZombieModpackWorld.biomeDesertedLands);
    	EntityRegistry.addSpawn(UndeadPig.class, 4, 1, 1, EnumCreatureType.monster, new BiomeGenBase[]{BiomeGenBase.deepOcean, BiomeGenBase.ocean, ZombieModpackWorld.biomeDesertedFlat, ZombieModpackWorld.biomeDesertedHills, ZombieModpackWorld.biomeDesertedLands});
    	EntityRegistry.addSpawn(UndeadCow.class, 4, 1, 1, EnumCreatureType.monster, new BiomeGenBase[]{BiomeGenBase.deepOcean, BiomeGenBase.ocean, ZombieModpackWorld.biomeDesertedFlat, ZombieModpackWorld.biomeDesertedHills, ZombieModpackWorld.biomeDesertedLands});
    	EntityRegistry.addSpawn(UndeadChicken.class, 4, 1, 1, EnumCreatureType.monster, new BiomeGenBase[]{BiomeGenBase.deepOcean, BiomeGenBase.ocean, ZombieModpackWorld.biomeDesertedFlat, ZombieModpackWorld.biomeDesertedHills, ZombieModpackWorld.biomeDesertedLands});
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
    	//potionZombieBite = new PotionZombieBite(32, false, 0).setIconIndex(6, 0);
    	//MinecraftForge.EVENT_BUS.register(new ZombieBiteEventHook());
    	MinecraftForge.EVENT_BUS.register(new LivingEntityHandler());
    	MinecraftForge.EVENT_BUS.register(new NoiseEventHook());
    	
    	
    	proxy.registerRenderers();
    }
    
}
