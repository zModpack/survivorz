package at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.Renderer;

import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.ZombieCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.Models.ModelChickenCorpse;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class RenderChickenCorpse extends RenderLiving {

	private ResourceLocation texture;
	
	public RenderChickenCorpse() {
		super(new ModelChickenCorpse(), 0.5F);
	}

	@Override
    protected ResourceLocation getEntityTexture(Entity par1Entity) {
        return ((ZombieCorpse) par1Entity).getTexture();
    }
	
}
