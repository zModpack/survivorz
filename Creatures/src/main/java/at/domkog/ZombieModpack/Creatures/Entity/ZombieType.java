package at.domkog.ZombieModpack.Creatures.Entity;

import at.domkog.ZombieModpack.Creatures.Creatures;
import net.minecraft.potion.PotionEffect;

public enum ZombieType {
	//Zombies:
	UNDEADCITIZEN("Zombie", new String[]{"Zombie_2", "Zombie_3"}, new int[]{20000, 20000}, 20000, ZombieType.getDefaultPotionEffects()),
	UNDEADWORKER("ZombieWorker", new String[]{"ZombieWorker_2", "ZombieWorker_3"}, new int[]{20000, 20000}, 20000, ZombieType.getDefaultPotionEffects()),
	UNDEADPOLICEMAN("ZombiePoliceman", new String[]{"ZombiePoliceman_2", "ZombiePoliceman_3"}, new int[]{20000, 20000}, 20000, ZombieType.getDefaultPotionEffects()),
	//Zombie animals:
	UNDEADPIG("UndeadPig", new String[]{"UndeadPig", "UndeadPig"}, new int[]{20000, 20000}, 20000, ZombieType.getDefaultPotionEffects()),
	UNDEADCOW("UndeadCow", new String[]{"UndeadCow", "UndeadCow"}, new int[]{20000, 20000}, 20000, ZombieType.getDefaultPotionEffects()),
	UNDEADCHICKEN("UndeadChicken", new String[]{"UndeadChicken", "UndeadChicken"}, new int[]{20000, 20000}, 20000, ZombieType.getDefaultPotionEffects()),
	UNDEADSHEEP("UndeadSheep", new String[]{"UndeadSheep", "UndeadSheep"}, new int[]{20000, 20000}, 20000, ZombieType.getDefaultPotionEffects());
	
	private String texturePath;
	private String[] decayPaths;
	private int[] decayTicks;
	private int destructionTicks;
	private PotionEffect[][] potionEffects;
	
	private ZombieType(String textureName, String[] decayTextures, int[] decayTicks, int destruction, PotionEffect[][] potionEffects) {
		this.texturePath = "zombiemodpackcreatures:textures/entities/" + textureName + ".png";
		String[] paths = new String[decayTextures.length];
		for(int i = 0; i < decayTextures.length; i++) {
			paths[i] = "zombiemodpackcreatures:textures/entities/" + decayTextures[i] + ".png";
		}
		this.decayPaths = paths;
		this.decayTicks = decayTicks;
		this.destructionTicks = destruction;
		this.potionEffects = potionEffects;
	}
	
	public String getTextureLocation() {
		return this.texturePath;
	}
	
	public String getDecayTexture(int index) {
		return this.decayPaths[index];
	}
	
	public int getDecayTicks(int index) {
		return this.decayTicks[index];
	}
	
	public int getDestructionTicks() {
		return this.destructionTicks;
	}
	
	public int getDecayStages() {
		return this.decayPaths.length;
	}
	
	public String[] getDecayTextures() {
		return this.decayPaths;
	}
	
	public PotionEffect[] getPotionEffects(int index) {
		return this.potionEffects[index];
	}
	
	public int getPotionEffectLength(int index) {
		return this.getPotionEffects(index).length;
	}
	
	public static PotionEffect[][] getDefaultPotionEffects() {
		return new PotionEffect[][]{new PotionEffect[]{/*new PotionEffect(Creatures.potionZombieBite.id, 3600, 1, false)*/}, new PotionEffect[]{new PotionEffect(9, 20, 5, false)}, new PotionEffect[]{new PotionEffect(9, 200, 5, false)}};
	}
	
	public static ZombieType fromName(String name) {
		if(name.equalsIgnoreCase("UNDEADCITIZEN")) return ZombieType.UNDEADCITIZEN;
		else if(name.equalsIgnoreCase("UNDEADWORKER")) return ZombieType.UNDEADWORKER;
		else if(name.equalsIgnoreCase("UNDEADPOLICEMAN")) return ZombieType.UNDEADPOLICEMAN;
		return null;
	}
	
}
