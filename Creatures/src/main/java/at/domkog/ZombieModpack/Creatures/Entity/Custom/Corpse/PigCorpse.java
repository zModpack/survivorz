package at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse;

import net.minecraft.world.World;
import at.domkog.ZombieModpack.Creatures.Entity.ZombieType;

public class PigCorpse extends ZombieCorpse {

	public PigCorpse(World p_i1738_1_) {
		super(p_i1738_1_);
		this.setSize(1f, 1f);
	}

	@Override
	public ZombieType getType() {
		return ZombieType.UNDEADPIG;
	}

}