package at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.Renderer;

import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.ZombieCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.Models.ModelPigCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.Models.ModelSheepCorpse1;

public class RenderSheepCorpse extends RenderLiving {

	private ResourceLocation texture;
	
	public RenderSheepCorpse() {
		super(new ModelSheepCorpse1(), 0.5F);
	}

	@Override
    protected ResourceLocation getEntityTexture(Entity par1Entity) {
        return ((ZombieCorpse) par1Entity).getTexture();
    }
	
}
