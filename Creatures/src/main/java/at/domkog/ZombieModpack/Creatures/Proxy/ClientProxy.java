package at.domkog.ZombieModpack.Creatures.Proxy;

import at.domkog.ZombieModpack.Creatures.Entity.Custom.UndeadCitizen;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.UndeadPoliceman;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.UndeadWorker;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Animals.UndeadChicken;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Animals.UndeadCow;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Animals.UndeadPig;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Animals.UndeadSheep;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.ChickenCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.CitizenCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.CowCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.PigCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.PolicemanCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.SheepCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.WorkerCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.Renderer.RenderChickenCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.Renderer.RenderCowCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.Renderer.RenderPigCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.Renderer.RenderSheepCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.Renderer.RenderZombieCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Render.RenderUndeadChicken;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Render.RenderUndeadCitizen;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Render.RenderUndeadCow;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Render.RenderUndeadPig;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Render.RenderUndeadPoliceman;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Render.RenderUndeadSheep;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Render.RenderUndeadWorker;
import cpw.mods.fml.client.registry.RenderingRegistry;

public class ClientProxy extends CommonProxy {

	public void registerRenderers() {
		//Zombie renderer:
		RenderingRegistry.registerEntityRenderingHandler(UndeadCitizen.class, new RenderUndeadCitizen());
		RenderingRegistry.registerEntityRenderingHandler(UndeadWorker.class, new RenderUndeadWorker());
		RenderingRegistry.registerEntityRenderingHandler(UndeadPoliceman.class, new RenderUndeadPoliceman());
		RenderingRegistry.registerEntityRenderingHandler(UndeadPig.class, new RenderUndeadPig());
		RenderingRegistry.registerEntityRenderingHandler(UndeadCow.class, new RenderUndeadCow());
		RenderingRegistry.registerEntityRenderingHandler(UndeadChicken.class, new RenderUndeadChicken());
		RenderingRegistry.registerEntityRenderingHandler(UndeadSheep.class, new RenderUndeadSheep());
		
		//Corpse renderer
		RenderingRegistry.registerEntityRenderingHandler(CitizenCorpse.class, new RenderZombieCorpse());
		RenderingRegistry.registerEntityRenderingHandler(WorkerCorpse.class, new RenderZombieCorpse());
		RenderingRegistry.registerEntityRenderingHandler(PolicemanCorpse.class, new RenderZombieCorpse());
		RenderingRegistry.registerEntityRenderingHandler(PigCorpse.class, new RenderPigCorpse());
		RenderingRegistry.registerEntityRenderingHandler(CowCorpse.class, new RenderCowCorpse());
		RenderingRegistry.registerEntityRenderingHandler(ChickenCorpse.class, new RenderChickenCorpse());
		RenderingRegistry.registerEntityRenderingHandler(SheepCorpse.class, new RenderSheepCorpse());
	}
	
}
