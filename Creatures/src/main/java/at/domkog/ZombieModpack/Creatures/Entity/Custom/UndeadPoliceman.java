package at.domkog.ZombieModpack.Creatures.Entity.Custom;

import net.minecraft.init.Items;
import net.minecraft.world.World;
import at.domkog.ZombieModpack.Creatures.Entity.CreatureAttributes;
import at.domkog.ZombieModpack.Creatures.Entity.CreatureZombie;
import at.domkog.ZombieModpack.Creatures.Entity.ZombieType;
import at.domkog.ZombieModpack.Creatures.Entity.AI.Noise.EntityAINoise;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.PolicemanCorpse;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.ZombieCorpse;

public class UndeadPoliceman extends CreatureZombie {

	public UndeadPoliceman(World p_i1745_1_) {
		super(p_i1745_1_);
		this.tasks.addTask(10, new EntityAINoise(this, 40));
		this.applyEntityAttributes(new CreatureAttributes().setAttackDamage(3D).setFollowRange(40D).setKnockbackResistance(0.1D).setMaxHealth(20D).setMovementSpeed(0.23000000417232513D));
	}

	@Override
	protected void dropFewItems(boolean par1, int par2) {
		if(this.rand.nextInt(3) == 0) {
			this.dropItem(Items.bone, 1);
			if(this.rand.nextInt(2) == 0) this.dropItem(Items.gunpowder, 1);
			//Drop armor, Flan's mod weapon
		}
	}

	@Override
	public void dropRareDrop(int p_70600_1_) {}

	@Override
	public void spawnCorpse() {
		PolicemanCorpse corpse = new PolicemanCorpse(worldObj);
		corpse.setLocationAndAngles(posX, posY, posZ, this.rand.nextFloat() * 360.0F, 0.0F);
		worldObj.spawnEntityInWorld(corpse);
	}
	
}
