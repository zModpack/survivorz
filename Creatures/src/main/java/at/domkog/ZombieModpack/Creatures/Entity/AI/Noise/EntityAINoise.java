package at.domkog.ZombieModpack.Creatures.Entity.AI.Noise;

import java.util.ListIterator;

import org.lwjgl.Sys;

import at.domkog.ZombieModpack.Creatures.Entity.CreatureZombie;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.world.WorldSettings.GameType;

public class EntityAINoise extends EntityAIBase {

	private CreatureZombie entity;
	private int range;
	
	private EntityPlayerMP target;
	
	public EntityAINoise(CreatureZombie e, int range) {
		this.entity = e;
		this.range = range;
	}
	
	@Override
	public boolean shouldExecute() {
		ListIterator listIterator = entity.worldObj.playerEntities.listIterator();
		while(listIterator.hasNext()) {
			EntityPlayerMP player = (EntityPlayerMP) listIterator.next();
			int newRange = (int) (range + (((float) range * player.getDataWatcher().getWatchableObjectFloat(20)) / 2));
			if(entity.getDistance(player.posX, player.posY, player.posZ) <= newRange && player.theItemInWorldManager.getGameType() != GameType.CREATIVE) {
				this.target = player;
				return true;
			} else return false;
		}
		return false;
	}

	@Override
	public void startExecuting() {
		if(this.target != null) {
			System.out.println("Targeted " + target.getDisplayName());
			this.entity.setTarget(target);
		}
	}
	
}
