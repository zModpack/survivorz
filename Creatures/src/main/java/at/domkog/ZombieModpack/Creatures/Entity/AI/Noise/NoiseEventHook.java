package at.domkog.ZombieModpack.Creatures.Entity.AI.Noise;

import java.lang.reflect.Field;
import java.util.HashMap;

import org.lwjgl.Sys;

import at.domkog.ZombieModpack.Creatures.Creatures;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.event.entity.EntityEvent.EntityConstructing;
import net.minecraftforge.event.entity.living.LivingEvent.LivingJumpEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerUseItemEvent;
import net.minecraftforge.event.world.BlockEvent.BreakEvent;
import net.minecraftforge.event.world.BlockEvent.PlaceEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class NoiseEventHook {

	private HashMap<String, Double> lastX = new HashMap<String, Double>();
	private HashMap<String, Double> lastY = new HashMap<String, Double>();
	private HashMap<String, Double> lastZ = new HashMap<String, Double>();
	
	@SubscribeEvent
	public void onEntityConstruction(EntityConstructing event){
		if(event.entity instanceof EntityPlayer) {
			EntityPlayer player = (EntityPlayer) event.entity;
			player.getDataWatcher().addObject(20, 0.0f);
		}
	}
	
	@SubscribeEvent
	public void onLivingUpdate(LivingUpdateEvent event) {
		if(event.entity instanceof EntityPlayerMP) {
			EntityPlayerMP player = (EntityPlayerMP) event.entity;
			String UUID = player.getUniqueID().toString();
			if(!lastX.containsKey(UUID)) lastX.put(UUID, player.posX);
			if(!lastY.containsKey(UUID)) lastY.put(UUID, player.posY);
			if(!lastZ.containsKey(UUID)) lastZ.put(UUID, player.posZ);
			//Check if player moved
			if(!NoiseData.hasData(player)) NoiseData.createData(player);
			if(lastX.get(UUID) != player.posX || lastZ.get(UUID) != player.posZ) {
				this.onPlayerMove(player);
				lastX.put(UUID, player.posX);
				lastZ.put(UUID, player.posZ);
			}
			else {
				float moveNoise = NoiseData.getData(player).getNoise(0);
				if(moveNoise != 0) moveNoise -= 0.025f;
				if(moveNoise < 0) moveNoise = 0;
				NoiseData.getData(player).setNoise(0, moveNoise);
			}
			if(lastY.get(UUID) != player.posY && player.onGround) {
				this.onPlayerJump(player);
				lastY.put(UUID, player.posY);
			} else {
				float jumpNoise = NoiseData.getData(player).getNoise(1);
				if(jumpNoise != 0) jumpNoise -= 0.025f;
				if(jumpNoise < 0) jumpNoise = 0;
				NoiseData.getData(player).setNoise(1, jumpNoise);
			}
			
			for(int i = 2; i < NoiseData.getData(player).getNoises().length; i++) {
				float tempNoise = NoiseData.getData(player).getNoise(i);
				if(tempNoise != 0) tempNoise -= 0.025f;
				if(tempNoise < 0) tempNoise = 0;
				NoiseData.getData(player).setNoise(i, tempNoise);
			}
			
			player.getDataWatcher().updateObject(20, NoiseData.getData(player).sumNoise());
		}
	}
	
	@SubscribeEvent
	public void onPlayerAttack(AttackEntityEvent event) {
		EntityPlayer player = event.entityPlayer;
		NoiseData noiseData = NoiseData.getData(player.getUniqueID().toString());
		if(!player.worldObj.isRemote) noiseData.setNoise(4, 0.2f);
		if(!player.worldObj.isRemote) player.getDataWatcher().updateObject(20, noiseData.sumNoise());
	}
	
	@SubscribeEvent
	public void onPlayerInteract(PlayerEvent.BreakSpeed event) {
		EntityPlayer player = event.entityPlayer;
		NoiseData noiseData = NoiseData.getData(player.getUniqueID().toString());
		if(!player.worldObj.isRemote) noiseData.setNoise(2, 0.125f);
		if(!player.worldObj.isRemote) player.getDataWatcher().updateObject(20, noiseData.sumNoise());
	}
	
	@SubscribeEvent
	public void onLivingJump(LivingJumpEvent event) {
		if(event.entity instanceof EntityPlayerMP) {
			this.onPlayerJump((EntityPlayerMP) event.entity);
		}
	}
	
	@SubscribeEvent
	public void onBreakBlock(BreakEvent event) {
		EntityPlayer player = event.getPlayer();
		String UUID = player.getUniqueID().toString();
		if(!player.worldObj.isRemote) NoiseData.getData(UUID).setNoise(2, 0.25f);
		if(!player.worldObj.isRemote) player.getDataWatcher().updateObject(20, NoiseData.getData(UUID).sumNoise());
	}
	
	@SubscribeEvent
	public void onPlaceBlock(PlaceEvent event) {
		EntityPlayer player = event.player;
		String UUID = player.getUniqueID().toString();
		if(!player.worldObj.isRemote) NoiseData.getData(UUID).setNoise(3, 0.25f);
		if(!player.worldObj.isRemote) player.getDataWatcher().updateObject(20, NoiseData.getData(UUID).sumNoise());
	}
	
	@SubscribeEvent
	@SideOnly(Side.CLIENT)
	public void overlayRenderEvent(RenderGameOverlayEvent event) {
		if(event.type != RenderGameOverlayEvent.ElementType.ALL) return;
		NoiseGUI.drawNoiseGui(event);
	}
	
	private void onPlayerMove(EntityPlayerMP player) {
		if(player.isSprinting()) NoiseData.getData(player).setNoise(0, 0.25f);
		else if(player.isSneaking()) NoiseData.getData(player).setNoise(0, 0.125f);
		else NoiseData.getData(player).setNoise(0, 0.2f);
	}
	
	private void onPlayerJump(EntityPlayerMP player) {
		NoiseData.getData(player).setNoise(1, 0.25f);
	}
	
}
