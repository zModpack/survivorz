package at.domkog.ZombieModpack.Creatures.Entity.AI.Noise;

import org.lwjgl.Sys;
import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiIngame;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.event.RenderGameOverlayEvent;

public class NoiseGUI {

	private static final ResourceLocation texture = new ResourceLocation("zombiemodpackcreatures", "textures/gui/Icons_Noise.png");
	private static Minecraft mc = Minecraft.getMinecraft();
	
	public static void drawNoiseGui(RenderGameOverlayEvent event) {
		
		GuiIngame gui = mc.ingameGUI;
		ScaledResolution scaledresolution = new ScaledResolution(mc, mc.displayWidth, mc.displayHeight);
        int k = scaledresolution.getScaledWidth();
        int l = scaledresolution.getScaledHeight();
        
		EntityClientPlayerMP player = Minecraft.getMinecraft().thePlayer;
		float noise = 0.0f;
		noise = player.getDataWatcher().getWatchableObjectFloat(20);
		int height = (int) ((float) 100 * noise);
		
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.getTextureManager().bindTexture(texture);
		gui.drawTexturedModalRect(k - 10, l - 105, 0, 0, 5, 100);
		gui.drawTexturedModalRect(k - 10, l - 105 + (100 - height), 5, (100 - height), 5, height);
	}
	
}
