package at.domkog.ZombieModpack.Creatures.Entity;

import java.util.Random;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.EntityEvent.EntityConstructing;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.living.LivingSpawnEvent;
import at.domkog.ZombieModpack.Creatures.Entity.AI.Noise.NoiseData;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.UndeadCitizen;
import at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse.ZombieCorpse;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class LivingEntityHandler {

	@SubscribeEvent
	public void livingDeath(LivingDeathEvent event) {
		Entity e = event.entityLiving;
		if(e instanceof CreatureZombie) {
			CreatureZombie zombie = (CreatureZombie) e;
			World w = e.worldObj;
			if(!w.isRemote) {
				System.out.println("Zombie killed spawning new corpse.");
				zombie.spawnCorpse();
			}
		}
	}
	
	@SubscribeEvent
	public void livingAttack(LivingAttackEvent event) {
		if(event.entityLiving instanceof ZombieCorpse) {
			if(event.source == DamageSource.inFire || event.source == DamageSource.onFire || event.source == DamageSource.lava) {
				ZombieCorpse corpse = (ZombieCorpse) event.entityLiving;
				if(corpse.getBurningTime() >= 100) {
					corpse.worldObj.removeEntity(corpse);
					return;
				}
				if(!corpse.isBurning()) corpse.setFire(100);
				corpse.setBurningTime(corpse.getBurningTime() + 1);
			}
			event.setCanceled(true);
		}
	}
	
	@SubscribeEvent
	public void livingSpawn(EntityJoinWorldEvent event) {
		if(event.entity instanceof EntityPlayerMP) {
			EntityPlayerMP player = (EntityPlayerMP) event.entity;
			NoiseData.createData(player);
		}
	}
	
}
