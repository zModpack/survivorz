package at.domkog.ZombieModpack.Creatures.Entity.Custom.Corpse;

import net.minecraft.world.World;
import at.domkog.ZombieModpack.Creatures.Entity.ZombieType;

public class ChickenCorpse extends ZombieCorpse {

	public ChickenCorpse(World p_i1738_1_) {
		super(p_i1738_1_);
		this.setSize(0.5f, 0.65f);
	}

	@Override
	public ZombieType getType() {
		return ZombieType.UNDEADCHICKEN;
	}

}