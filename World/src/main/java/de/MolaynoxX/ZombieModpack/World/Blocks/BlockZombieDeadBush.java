package de.MolaynoxX.ZombieModpack.World.Blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDeadBush;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

public class BlockZombieDeadBush extends BlockDeadBush {

	public BlockZombieDeadBush() {
		super();
	}
	
	@Override
	protected boolean canPlaceBlockOn(Block p_149854_1_)
    {
        return p_149854_1_ == Blocks.sand || p_149854_1_ == Blocks.hardened_clay || p_149854_1_ == Blocks.stained_hardened_clay || p_149854_1_ == Blocks.dirt || p_149854_1_ == Blocks.grass;
    }
	
	@Override
	public boolean canBlockStay(World p_149718_1_, int p_149718_2_, int p_149718_3_, int p_149718_4_)
    {
        return  p_149718_1_.getBlock(p_149718_2_, p_149718_3_ - 1, p_149718_4_) == Blocks.grass;
    }
	
}
