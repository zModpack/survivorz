package de.MolaynoxX.ZombieModpack.World.Biome;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import de.MolaynoxX.ZombieModpack.World.World.ZombieFlatBiomeDecorator;
import net.minecraft.world.biome.BiomeDecorator;
import net.minecraft.world.biome.BiomeGenBase;

public class BiomeGenDesertedHills extends BiomeGenBase {

	public BiomeGenDesertedHills(int par1) {
		super(par1);
		this.rainfall = 10F;
		this.rootHeight = 1.2F;
		this.heightVariation = 0.7F;
		this.spawnableWaterCreatureList.clear();
		this.spawnableMonsterList.clear();
		this.spawnableCreatureList.clear();
		this.flowers.clear();
		this.setColor(0x0d581d);
		this.setBiomeName("Deserted Hills");
		//this.waterColorMultiplier = 0x5a2306;
	}
	
	@Override
	public BiomeDecorator createBiomeDecorator() {
		return new ZombieFlatBiomeDecorator();
	}

	@Override
	@SideOnly(Side.CLIENT)
    public int getBiomeGrassColor(int p_150558_1_, int p_150558_2_, int p_150558_3_) {
        return 0x5A6351;
    }
	
	@Override
	@SideOnly(Side.CLIENT)
    public int getBiomeFoliageColor(int p_150571_1_, int p_150571_2_, int p_150571_3_) {
		return 0x435D36;
	}
	
}
