package de.MolaynoxX.ZombieModpack.World.Blocks;

import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import de.MolaynoxX.ZombieModpack.World.ZombieModpackWorld;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.client.particle.EntitySpellParticleFX;
import net.minecraft.init.Blocks;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;

public class BlockRottenWater extends BlockFluidClassic {

	public BlockRottenWater() {
		super(ZombieModpackWorld.rottenWater, Material.water);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int meta) {
		return Blocks.water.getIcon(side, meta);
	}

	@Override
	public int colorMultiplier(IBlockAccess iblockaccess, int x, int y, int z) {
		return 0x663300;
	}
	
	@Override
    public boolean canDisplace(IBlockAccess world, int x, int y, int z) {
            if (world.getBlock(x,  y,  z).getMaterial().isLiquid()) return false;
            return super.canDisplace(world, x, y, z);
    }
    
    @Override
    public boolean displaceIfPossible(World world, int x, int y, int z) {
            if (world.getBlock(x,  y,  z).getMaterial().isLiquid()) return false;
            return super.displaceIfPossible(world, x, y, z);
    }
    
    @SideOnly(Side.CLIENT)
    public void randomDisplayTick(World worldObj, int posX, int posY, int posZ, Random rand)
    {
    	if (this.blockMaterial == Material.water && worldObj.getBlock(posX, posY + 1, posZ).getMaterial() == Material.air && !worldObj.getBlock(posX, posY + 1, posZ).isOpaqueCube())
    	{
    		int direction = worldObj.getBlockMetadata(posX, posY, posZ);

    		float x1 = (float)posX + rand.nextFloat();
    		float y1 = (float)posY + rand.nextFloat();
    		float z1 = (float)posZ + rand.nextFloat();

    		float f =  rand.nextFloat();
    		float f1 = rand.nextFloat();

    		if(rand.nextInt(4) == 0) {
    			worldObj.spawnParticle("smoke", posX, posY + 1, posZ, .1 * rand.nextDouble(), .2, .1 * rand.nextDouble());
    		} else if(rand.nextInt(4) == 0) {
    			EntityFX spellParticle = new EntitySpellParticleFX(worldObj, posX, posY + 1, posZ, 0, 0.1, 0);
	    		spellParticle.setRBGColorF(.64F, .46F, .28F);
	    		
	    		Minecraft.getMinecraft().effectRenderer.addEffect(spellParticle);
    		} else if(rand.nextInt(6) == 0) {
    			worldObj.spawnParticle("smoke", posX, posY + 1, posZ, .1 * rand.nextDouble(), .2, .1 * rand.nextDouble());  
    		}
    		//worldObj.spawnParticle("spell", posX, posY + 1, posZ, .0, .1, .0);    	    

    	}
    }
	
}
