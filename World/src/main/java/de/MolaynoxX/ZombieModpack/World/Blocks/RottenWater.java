package de.MolaynoxX.ZombieModpack.World.Blocks;

import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;

public class RottenWater extends Fluid {

	public RottenWater() {
		super("rottenWater");
		
		this.setDensity(10);
		this.setViscosity(1000);
		FluidRegistry.registerFluid(this);
	}

}
