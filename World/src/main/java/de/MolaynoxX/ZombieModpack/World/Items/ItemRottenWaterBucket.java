package de.MolaynoxX.ZombieModpack.World.Items;

import cpw.mods.fml.common.eventhandler.Event;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBucket;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.FillBucketEvent;
import net.minecraftforge.fluids.ItemFluidContainer;

public class ItemRottenWaterBucket extends ItemBucket {

	
	public ItemRottenWaterBucket(Block p_i45331_1_) {
		super(p_i45331_1_);
		this.setUnlocalizedName("bucketRottenWater");
	}
	
}
