package de.MolaynoxX.ZombieModpack.World.Events;

import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.init.Blocks;
import net.minecraft.world.ChunkCoordIntPair;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.event.terraingen.PopulateChunkEvent;
import net.minecraftforge.event.world.ChunkEvent;

public class BedrockEventHooks {

	@SubscribeEvent(priority=EventPriority.HIGH)
	public void onPostPopulateChunk(PopulateChunkEvent.Post event)
	{
		World world = event.world;
		Chunk chunk = world.getChunkFromChunkCoords(event.chunkX, event.chunkZ);
		for (int x = 0; x < 16; x++) {
			for (int z = 0; z < 16; z++) {
				for (int y = 1; y < 50; y++) {
					chunk.func_150807_a(x, y, z, Blocks.bedrock, 0);
				}
			}
		}
	}
}
