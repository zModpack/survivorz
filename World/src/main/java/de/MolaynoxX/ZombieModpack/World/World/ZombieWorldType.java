package de.MolaynoxX.ZombieModpack.World.World;

import net.minecraft.world.World;
import net.minecraft.world.WorldType;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.biome.WorldChunkManager;
import net.minecraft.world.biome.WorldChunkManagerHell;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.layer.GenLayer;
import net.minecraft.world.gen.layer.GenLayerBiome;
import net.minecraft.world.gen.layer.GenLayerBiomeEdge;
import net.minecraft.world.gen.layer.GenLayerZoom;

public class ZombieWorldType extends WorldType {

	public ZombieWorldType(String name) {
		super(name);
	}
	
	public WorldChunkManager getChunkManager(World world) {
        //return new WorldChunkManagerHell(new BiomeGenDeserted(60), 1F);
		
		return new ZombieChunkManager(world);
    }

    public IChunkProvider getChunkGenerator(World world, String generatorOptions) {
        return new ZombieChunkGenerator(world, world.getSeed());
    }
	
    public float getCloudHeight() {
        return 256F;
    }
	
    @Override
    public GenLayer getBiomeLayer(long worldSeed, GenLayer parentLayer) {
        GenLayer ret = new ZombieGenLayerBiome(200L, parentLayer, this);
        ret = GenLayerZoom.magnify(1000L, ret, 2);
        ret = new GenLayerBiomeEdge(1000L, ret);
        return ret;
    }
    
    public double getHorizon(World world) {
        return 0D;
    }
    
}
