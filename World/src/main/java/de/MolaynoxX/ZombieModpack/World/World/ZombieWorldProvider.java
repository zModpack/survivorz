package de.MolaynoxX.ZombieModpack.World.World;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.Entity;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.WorldProvider;
import net.minecraft.world.chunk.IChunkProvider;

public class ZombieWorldProvider extends WorldProvider {
	
	@Override
	public String getDimensionName() {
		return "Zombie World";
	}
	
	@Override
	public IChunkProvider createChunkGenerator() {
		return new ZombieChunkGenerator(this.worldObj, this.getSeed());
	}
	
	@Override
	public void registerWorldChunkManager() {
		this.worldChunkMgr = new ZombieChunkManager(this.worldObj);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public Vec3 getFogColor(float par1, float par2)
	{
		 /*float f2 = MathHelper.cos(par1 * (float)Math.PI * 2.0F) * 2.0F + 0.5F;

	        if (f2 < 0.0F)
	        {
	            f2 = 0.0F;
	        }

	        if (f2 > 1.0F)
	        {
	            f2 = 1.0F;
	        }

	        float f3 = 0.7529412F;
	        float f4 = 0.84705883F;
	        float f5 = 1.0F;
	        f3 *= f2 * 0.94F + 0.06F;
	        f4 *= f2 * 0.94F + 0.06F;
	        f5 *= f2 * 0.91F + 0.09F;
	        return Vec3.createVectorHelper((double)f3, (double)f4, (double)f5);*/
		return Vec3.createVectorHelper(.26, .39, .50);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public Vec3 getSkyColor(Entity cameraEntity, float partialTicks) {
		//return worldObj.getSkyColorBody(cameraEntity, partialTicks);
		return Vec3.createVectorHelper(.26, .39, .50);
	}
	
	@SideOnly(Side.CLIENT)
	public boolean isSkyColored()
	{
		return true;
	}

}
