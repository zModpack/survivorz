package de.MolaynoxX.ZombieModpack.World.Blocks;

import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;

public class FiniteWater extends Fluid {

	public FiniteWater() {
		super("finiteWater");
		
		FluidRegistry.registerFluid(this);
	}

}
