package de.MolaynoxX.ZombieModpack.World;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidRegistry;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import de.MolaynoxX.ZombieModpack.World.Biome.BiomeGenDeserted;
import de.MolaynoxX.ZombieModpack.World.Biome.BiomeGenDesertedFlat;
import de.MolaynoxX.ZombieModpack.World.Biome.BiomeGenDesertedHills;
import de.MolaynoxX.ZombieModpack.World.Blocks.BlockFiniteWater;
import de.MolaynoxX.ZombieModpack.World.Blocks.BlockRottenWater;
import de.MolaynoxX.ZombieModpack.World.Blocks.BlockZombieDeadBush;
import de.MolaynoxX.ZombieModpack.World.Blocks.FiniteWater;
import de.MolaynoxX.ZombieModpack.World.Blocks.RottenWater;
import de.MolaynoxX.ZombieModpack.World.Events.BedrockEventHooks;
import de.MolaynoxX.ZombieModpack.World.Events.BucketEventHooks;
import de.MolaynoxX.ZombieModpack.World.Items.ItemFiniteWaterBucket;
import de.MolaynoxX.ZombieModpack.World.Items.ItemRottenWaterBucket;
import de.MolaynoxX.ZombieModpack.World.World.ZombieWorldProvider;
import de.MolaynoxX.ZombieModpack.World.World.ZombieWorldType;

@Mod(modid = ZombieModpackWorld.MODID, version = ZombieModpackWorld.VERSION)
public class ZombieModpackWorld {

	public static final String MODID = "ZombieModpackWorld";
    public static final String VERSION = "InDev-0.2.0";
    
    public static final int zombieDimensionId = 0;
    public static final ZombieWorldType worldType = new ZombieWorldType("ZombieModpack");
    
    public static final BiomeGenBase biomeDesertedLands = new BiomeGenDeserted(60);
    public static final BiomeGenBase biomeDesertedHills = new BiomeGenDesertedHills(61);
    public static final BiomeGenBase biomeDesertedFlat = new BiomeGenDesertedFlat(62);
    
    public static final Block zombieDeadBush = new BlockZombieDeadBush().setBlockName("zombieDeadBush").setBlockTextureName("deadbush");
    
    public static final Fluid rottenWater = new RottenWater();
    public static final Block rottenWaterSolid = new BlockRottenWater().setBlockName("rottenWaterSolid");
    public static final Fluid finiteWater = new FiniteWater();
    public static final Block finiteWaterSolid = new BlockFiniteWater().setBlockName("finiteWaterSolid");
    
    public static final Item rottenWaterBucket = new ItemRottenWaterBucket(rottenWaterSolid).setTextureName(MODID + ":" + "bucketRottenWater");
    public static final Item finiteWaterBucket = new ItemFiniteWaterBucket(finiteWaterSolid).setTextureName("bucket_water").setUnlocalizedName("bucketWater");
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
		MinecraftForge.EVENT_BUS.register(new BedrockEventHooks());
		MinecraftForge.EVENT_BUS.register(new BucketEventHooks());
		
		DimensionManager.unregisterProviderType(zombieDimensionId);
		DimensionManager.registerProviderType(zombieDimensionId, ZombieWorldProvider.class, true);
				
		GameRegistry.registerBlock(zombieDeadBush, "zombieDeadBush");
		GameRegistry.registerBlock(rottenWaterSolid, "rottenWaterSolid");
		GameRegistry.registerBlock(finiteWaterSolid, "finiteWaterSolid");
		
		GameRegistry.registerItem(rottenWaterBucket, "bucketRottenWater");
		GameRegistry.registerItem(finiteWaterBucket, "bucketWater");
		
		FluidContainerRegistry.registerFluidContainer(rottenWater, new ItemStack(rottenWaterBucket), new ItemStack(Items.bucket));
		FluidContainerRegistry.registerFluidContainer(finiteWater, new ItemStack(finiteWaterBucket), new ItemStack(Items.bucket));
		
    }
	
}
