package de.MolaynoxX.ZombieModpack.World.World;

import java.util.Random;

import de.MolaynoxX.ZombieModpack.World.ZombieModpackWorld;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;

public class WorldGenGrass extends WorldGenerator {

	@Override
	public boolean generate(World p_76484_1_, Random randGen, int p_76484_3_, int p_76484_4_, int p_76484_5_) {
		Block block;
		Block grass;
		int metadata = 0;
		
		do
		{
			block = p_76484_1_.getBlock(p_76484_3_, p_76484_4_, p_76484_5_);
			if (!(block.isLeaves(p_76484_1_, p_76484_3_, p_76484_4_, p_76484_5_) || block.isAir(p_76484_1_, p_76484_3_, p_76484_4_, p_76484_5_)))
			{
				break;
			}
			--p_76484_4_;
		} while (p_76484_4_ > 0);

		for (int l = 0; l < 128; ++l)
		{
			int i1 = p_76484_3_ + randGen.nextInt(8) - randGen.nextInt(8);
			int j1 = p_76484_4_ + randGen.nextInt(4) - randGen.nextInt(4);
			int k1 = p_76484_5_ + randGen.nextInt(8) - randGen.nextInt(8);

			if(randGen.nextInt(3) == 0) {
				grass = Blocks.double_plant;
				metadata = 2;
			} else if(randGen.nextInt(7) == 0) {
				grass = Blocks.double_plant;
				metadata = 3;
			} else if(randGen.nextInt(5) == 0) {
				grass = ZombieModpackWorld.zombieDeadBush;
			} else {
				continue;
			}
			
			if (p_76484_1_.isAirBlock(i1, j1, k1) && grass.canBlockStay(p_76484_1_, i1, j1, k1))
			{
				p_76484_1_.setBlock(i1, j1, k1, grass, metadata, 2);
				if(grass == Blocks.double_plant) {
					p_76484_1_.setBlock(i1, j1 + 1, k1, grass, 8, 2);
				}
			}
		}

		return true;
	}

}
