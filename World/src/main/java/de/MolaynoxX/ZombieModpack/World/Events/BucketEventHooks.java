package de.MolaynoxX.ZombieModpack.World.Events;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.player.FillBucketEvent;
import cpw.mods.fml.common.eventhandler.Event.Result;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import de.MolaynoxX.ZombieModpack.World.ZombieModpackWorld;

public class BucketEventHooks {

	@SubscribeEvent
	public void onBucketFill(FillBucketEvent event) {
		Block b = event.world.getBlock(event.target.blockX, event.target.blockY, event.target.blockZ);
		if(b == ZombieModpackWorld.rottenWaterSolid && event.world.getBlockMetadata(event.target.blockX, event.target.blockY, event.target.blockZ) == 0) {
			event.world.setBlockToAir(event.target.blockX, event.target.blockY, event.target.blockZ);
			event.result = new ItemStack(ZombieModpackWorld.rottenWaterBucket);
			event.setResult(Result.ALLOW);
		} else if(b == ZombieModpackWorld.finiteWaterSolid && event.world.getBlockMetadata(event.target.blockX, event.target.blockY, event.target.blockZ) == 0) {
			event.world.setBlockToAir(event.target.blockX, event.target.blockY, event.target.blockZ);
			event.result = new ItemStack(ZombieModpackWorld.finiteWaterBucket);
			event.setResult(Result.ALLOW);
		}
	}

}
